﻿using IdentityServer4.Stores;
using System.Threading.Tasks;

namespace Identity.Api.Extensions
{
    public static class ClientExtensions
    { 
        /// <summary>
      /// 判断客户端是否配置使用PKCE.
      /// </summary>
      /// <param name="store">store.</param>
      /// <param name="client_id">Client Id</param>
      /// <returns></returns>
        public static async Task<bool> IsPkceClientAsync(this IClientStore store, string client_id)
        {
            if (!string.IsNullOrWhiteSpace(client_id))
            {
                var client = await store.FindEnabledClientByIdAsync(client_id);
                return client?.RequirePkce == true;
            }

            return false;
        }
    }
}
