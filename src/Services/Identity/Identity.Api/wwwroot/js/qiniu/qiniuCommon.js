﻿var BLOCK_SIZE = 4 * 1024 * 1024;

function addUploadBoard(file, config, key, type) {
    var count = Math.ceil(file.size / BLOCK_SIZE);
    //再次选择文件清空
    $("#fsUploadProgress" + type).html("");
    var board = widget.add("tr", {
        data: { num: count, name: key, size: file.size },
        node: $("#fsUploadProgress" + type)
    });
    if (file.size > 100 * 1024 * 1024) {
        $(board).html("本实例最大上传文件100M");
        return "";
    }
    count > 1 && type != "3"
        ? ""
        : $(board)
            .find(".resume")
            .addClass("hide");
    return board;
}

