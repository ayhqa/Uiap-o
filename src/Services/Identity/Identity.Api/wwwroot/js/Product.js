﻿var _table = $('#ProductTable');
//初始化bootstrap table ，并且启动它
function init() {
    _table.bootstrapTable({
        url: '/products/GetData',
        method: 'get',
        pagination: true,  //表格底部显示分页条
        sidePagination: "server",
        escape: false, //启动转义字符
        pageSize: 10, //每页显示多少条数据
        pageNumber: 1, //初始化翻页的页码
        pageList: [10, 25, 50, 100],
        queryParamsType: '',//设置请求参数格式
        queryParams: function queryParams(params) {   //设自定义查询参数
            var param = {
                limit: params.pageSize,   //页面大小
                offset: params.pageNumber
            };
            return param;
        }, columns: [
            {
                field: 'state',
                checkbox: true
            }, {
                field: 'ID',
                title: 'ID'
            }, {
                field: 'Name',
                title: '名字'
            }, {
                field: 'Sex',
                title: '性别'
            },
            {
                field: 'operate',
                title: '操作',
                formatter: operateFormatter //自定义方法，添加操作按钮
            },
        ]
    });

};


$(function () {
    init();
})

function operateFormatter(value, row, index) {
    return [
        '<a class="edit" href="javascript:void(0)" title="编辑">',
        '<i class="glyphicon glyphicon-edit"></i>',
        '</a>  ',
        '<a class="cancel" href="javascript:void(0)" title="删除">',
        '<i class="glyphicon glyphicon-remove"></i>',
        '</a>'
    ].join('');
}