﻿using System.IO;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;

namespace Identity.Api.Certificates
{
    public static class Certificate
    {
        public static X509Certificate2 Get()
        {
            var assembly = typeof(Certificate).GetTypeInfo().Assembly;
            var names = assembly.GetManifestResourceNames();

            /***********************************************************************************************
             *  注意：开发测试时可以把证书放在项目目录
             *  真实环境中，证书目录应该放在项目目录之外的其他目录
             **********************************************************************************************/
            using (var stream = assembly.GetManifestResourceStream("Identity.Api.Certificates.idsrv3test.pfx"))
            {
                return new X509Certificate2(ReadStream(stream), "idsrv3test");
            }
        }

        private static byte[] ReadStream(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}
