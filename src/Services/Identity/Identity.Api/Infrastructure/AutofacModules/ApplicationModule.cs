﻿using Autofac;
using Identity.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Identity.Api.Infrastructure.AutofacModules
{
    public class ApplicationModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //builder.Register(c => new DapperDbContext(ConnectionString)).InstancePerLifetimeScope();
            //builder.RegisterGeneric(typeof(EfRepository<,>)).As(typeof(IEfRepository<,>)).InstancePerLifetimeScope();
            //builder.RegisterGeneric(typeof(DapperRepository<,>)).As(typeof(IDapperRepository<,>)).InstancePerLifetimeScope();

            //builder.RegisterType<RedisHelper>().As<IRedisHelper>();
            //builder.RegisterType<LogHelper>().As<ILogHelper>();

            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            //Type repositoryType = typeof(IRepository<,>);
            //builder.RegisterAssemblyTypes(assemblies)
            //    .Where(t => repositoryType.IsAssignableFrom(t) && t.IsClass)
            //    .AsImplementedInterfaces().InstancePerLifetimeScope();

            Type basetype = typeof(IApplicationService);
            builder.RegisterAssemblyTypes(assemblies)
                .Where(t => basetype.IsAssignableFrom(t) && t.IsClass)
                .AsImplementedInterfaces().InstancePerLifetimeScope();
        }
    }
}
