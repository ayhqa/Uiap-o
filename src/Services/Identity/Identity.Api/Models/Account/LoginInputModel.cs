using System.ComponentModel.DataAnnotations;

namespace Identity.Api.Models.Account
{
    public class LoginInputModel
    {
        [Required(ErrorMessage ="�������¼����")]
        [Display(Name ="��¼����")]
        public string Email { get; set; }

        [Required(ErrorMessage ="�������¼����")]
        [Display(Name ="��¼����")]
        public string Password { get; set; }

        [Display(Name ="�Զ���¼")]
        public bool RememberMe { get; set; }
        public string ReturnUrl { get; set; }
    }
}