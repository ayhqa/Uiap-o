﻿
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace Identity.Domain.Entities.Account
{
    public class ApplicationUser : IdentityUser
    {
        /// <summary>
        /// 昵称（微信昵称）
        /// </summary>
        [Required(AllowEmptyStrings = true)]
        public string NickName { get; set; }

        /// <summary>
        /// 账号类型，<see cref="UserTypeEnum"/>
        /// </summary>
        [Required]
        public int UserType { get; set; }

        /// <summary>
        /// 性别，0男，1女
        /// </summary>
        [Required]
        public int Gender { get; set; }

        /// <summary>
        /// 微信OpenId
        /// </summary>
        [Required(AllowEmptyStrings = true)]
        public string OpenId { get; set; }

        /// <summary>
        /// 微信unionId
        /// </summary>
        [Required(AllowEmptyStrings = true)]
        public string UnionId { get; set; }

        /// <summary>
        /// 好友数，如果对方同时也关注了自己，则互为关注
        /// </summary>
        [Required]
        public int TotalFriends { get; set; }

        /// <summary>
        /// 关注数，如果对方同时也关注了自己，则互为关注
        /// </summary>
        [Required]
        public int TotalFollows { get; set; }

        /// <summary>
        /// 用户头像
        /// </summary>
        [Required]
        public string Avatar { get; set; }
    }
}
