﻿using System;
using System.Collections.Generic;
using System.Linq;
using Uiap.Infrastructure;
using Uiap.Infrastructure.SeedWork;

namespace Identity.Domain.Entities.Account
{
    public class GenderTypeEnum : Enumeration
    {
        /// <summary>
        /// 男
        /// </summary>
        public static GenderTypeEnum Male = new GenderTypeEnum(0, "男");

        /// <summary>
        /// 女
        /// </summary>
        public static GenderTypeEnum Female = new GenderTypeEnum(1, "女");

        /// <summary>
        /// 未知
        /// </summary>
        public static GenderTypeEnum Unknowed = new GenderTypeEnum(2, "未知");


        public GenderTypeEnum(int id, string name) : base(id, name)
        { }

        public static IEnumerable<GenderTypeEnum> List() =>
            new[] { Male, Female, Unknowed };

        public static GenderTypeEnum FromName(string name)
        {
            var state = List()
                .SingleOrDefault(s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));

            if (state == null)
            {
                throw new DomainException($"支持的性别类型为: {String.Join(",", List().Select(s => s.Name))}");
            }

            return state;
        }

        public static GenderTypeEnum From(int id)
        {
            var state = List().SingleOrDefault(s => s.Id == id);

            if (state == null)
            {
                throw new DomainException($"支持的性别类型为: {String.Join(",", List().Select(s => s.Name))}");
            }

            return state;
        }
    }
}
