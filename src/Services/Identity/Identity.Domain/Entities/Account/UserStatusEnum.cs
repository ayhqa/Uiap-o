﻿using System;
using System.Collections.Generic;
using System.Linq;
using Uiap.Infrastructure;
using Uiap.Infrastructure.SeedWork;

namespace Silverlining.Domain.Entities.Account
{
    public class UserStatusEnum : Enumeration
    {
        /// <summary>
        /// 未实名认证
        /// </summary>
        public static UserStatusEnum NotCertificate = new UserStatusEnum(1, "未实名认证");

        /// <summary>
        /// 申请了实名认证，但未获通过
        /// </summary>
        public static UserStatusEnum NotPassCertificate = new UserStatusEnum(2, "未通过实名认证");



        public UserStatusEnum(int id, string name) : base(id, name)
        {

        }

        public static IEnumerable<UserStatusEnum> List() =>
            new[] { NotCertificate, NotPassCertificate };

        public static UserStatusEnum FromName(string name)
        {
            var state = List()
                .SingleOrDefault(s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));

            if (state == null)
            {
                throw new DomainException($"支持的状态值为: {String.Join(",", List().Select(s => s.Name))}");
            }

            return state;
        }

        public static UserStatusEnum From(int id)
        {
            var state = List().SingleOrDefault(s => s.Id == id);

            if (state == null)
            {
                throw new DomainException($"支持的状态值为: {String.Join(",", List().Select(s => s.Name))}");
            }

            return state;
        }
    }
}
