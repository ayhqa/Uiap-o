﻿using System;
using System.Collections.Generic;
using System.Linq;
using Uiap.Infrastructure;
using Uiap.Infrastructure.SeedWork;

namespace Silverlining.Domain.Entities.Account
{
    /// <summary>
    /// 账号类型
    /// </summary>
    public class UserTypeEnum : Enumeration
    {
        /// <summary>
        /// 平台管理员
        /// </summary>
        public static UserTypeEnum System = new UserTypeEnum(1, "平台管理员");

        /// <summary>
        /// 普通用户
        /// </summary>
        public static UserTypeEnum Customer = new UserTypeEnum(2, "普通用户");


        public UserTypeEnum(int id, string name) : base(id, name)
        { }

        public static IEnumerable<UserTypeEnum> List() =>
            new[] { System, Customer };

        public static UserTypeEnum FromName(string name)
        {
            var state = List()
                .SingleOrDefault(s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));

            if (state == null)
            {
                throw new DomainException($"支持的账号类型为: {String.Join(",", List().Select(s => s.Name))}");
            }

            return state;
        }

        public static UserTypeEnum From(int id)
        {
            var state = List().SingleOrDefault(s => s.Id == id);

            if (state == null)
            {
                throw new DomainException($"支持的账号类型为: {String.Join(",", List().Select(s => s.Name))}");
            }

            return state;
        }
    }
}
