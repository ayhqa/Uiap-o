﻿using Identity.Domain.Entities.Account;
using Microsoft.AspNetCore.Authentication;
using System.Threading.Tasks;

namespace Identity.Service.Account
{
    public interface IAccountService : IApplicationService
    {
        /// <summary>
        /// 验证
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Task<bool> ValidateCredentials(ApplicationUser user, string password);

        /// <summary>
        /// 根据用户名查找用户信息
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ApplicationUser> FindByUsername(string user);

        /// <summary>
        /// 登入
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Task SignIn(ApplicationUser user);

        /// <summary>
        /// 异步登入
        /// </summary>
        /// <param name="user"></param>
        /// <param name="properties"></param>
        /// <param name="authenticationMethod"></param>
        /// <returns></returns>
        Task SignInAsync(ApplicationUser user, AuthenticationProperties properties, string authenticationMethod = null);
    }
}
