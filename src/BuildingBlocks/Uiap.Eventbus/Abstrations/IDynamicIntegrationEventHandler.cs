﻿using System.Threading.Tasks;

namespace Uiap.EventBus.Abstrations
{
    /// <summary>
    /// 动态事件处理器
    /// </summary>
    public interface IDynamicIntegrationEventHandler
    {
        /// <summary>
        /// 处理事件
        /// </summary>
        /// <param name="eventData"></param>
        /// <returns></returns>
        Task Handle(dynamic eventData);
    }
}
