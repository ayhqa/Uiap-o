﻿using Uiap.EventBus.Events;
using System.Threading.Tasks;

namespace Uiap.EventBus.Abstrations
{
    /// <summary>
    /// 集成事件处理器
    /// </summary>
    public interface IIntegrationEventHandler
    {

    }

    /// <summary>
    /// 泛型集成事件处理器（逆变）
    /// </summary>
    /// <typeparam name="TIntegrationEvent"></typeparam>
    public interface IIntegrationEventHandler<in TIntegrationEvent> : IIntegrationEventHandler
        where TIntegrationEvent : IntegrationEvent
    {
        /// <summary>
        /// 处理事件
        /// </summary>
        /// <param name="event"></param>
        /// <returns></returns>
        Task Handle(TIntegrationEvent @event);
    }
}
