﻿using Uiap.EventBus;
using Uiap.EventBus.Abstrations;
using Uiap.EventBus.Events;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Uiap.EventBus
{
    /// <summary>
    /// 基于Redis的分布式订阅管理器
    /// </summary>
    public class RedisEventBusSubscriptionsManager : IEventBusSubscriptionsManager
    {
        #region fields & properties

        /// <summary>
        /// 订阅（消费者）集合
        /// </summary>
        private readonly Dictionary<string, List<SubscriptionInfo>> _handlers;

        /// <summary>
        /// 事件类型列表
        /// </summary>
        private readonly List<Type> _eventTypes;

        /// <summary>
        /// 标识是否有需要处理的事件
        /// </summary>
        public bool IsEmpty => !_handlers.Any();

        public event EventHandler<string> OnEventRemoved;

        #endregion

        #region ctors

        public RedisEventBusSubscriptionsManager()
        {
            _handlers = new Dictionary<string, List<SubscriptionInfo>>();
            _eventTypes = new List<Type>();
        }

        #endregion

        #region methods

        public void AddDynamicSubscription<TH>(string eventName) where TH : IDynamicIntegrationEventHandler
        {
            throw new NotImplementedException();
        }

        public void AddSubscription<T, TH>()
            where T : IntegrationEvent
            where TH : IIntegrationEventHandler<T>
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public string GetEventName<T>()
        {
            throw new NotImplementedException();
        }

        public Type GetEventTypeByName(string eventName)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SubscriptionInfo> GetHandlerForEvent(string eventName)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SubscriptionInfo> GetHandlersForEvent<T>() where T : IntegrationEvent
        {
            throw new NotImplementedException();
        }

        public bool HasSubscriptionForEvent<T>() where T : IntegrationEvent
        {
            throw new NotImplementedException();
        }

        public bool HasSubscriptionForEvent(string eventName)
        {
            throw new NotImplementedException();
        }

        public void RemoveDynamicSubscription<TH>(string eventName) where TH : IDynamicIntegrationEventHandler
        {
            throw new NotImplementedException();
        }

        public void RemoveSubscription<T, TH>()
            where T : IntegrationEvent
            where TH : IIntegrationEventHandler<T>
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 调用移除方法
        /// </summary>
        /// <param name="eventName"></param>
        private void RasieOnEventRemoved(string eventName)
        {
            var handler = OnEventRemoved;
            handler?.Invoke(this, eventName);
        }

        #endregion
    }
}
