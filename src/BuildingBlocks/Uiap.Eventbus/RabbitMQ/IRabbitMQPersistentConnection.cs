﻿using RabbitMQ.Client;
using System;

namespace Uiap.EventBus.RabbitMQ
{
    /// <summary>
    /// 用于检查RabbitMQ的连接情况，确保连接持续
    /// </summary>
    public interface IRabbitMQPersistentConnection : IDisposable
    {
        /// <summary>
        /// 标识连接是否可用
        /// </summary>
        bool IsConnected { get; }

        /// <summary>
        /// 连接RabbitMQ
        /// </summary>
        /// <returns></returns>
        bool TryConnect();

        /// <summary>
        /// 创建AMQP对象
        /// </summary>
        /// <returns></returns>
        IModel CreateModel();
    }
}
