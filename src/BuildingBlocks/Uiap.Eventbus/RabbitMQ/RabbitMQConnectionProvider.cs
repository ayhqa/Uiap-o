﻿using RabbitMQ.Client;
using Uiap.Infrastructure.Configurations;

namespace Uiap.EventBus.RabbitMQ
{
    public class RabbitMQConnectionProvider
    {
        /// <summary>
        /// 实例化一个连接
        /// </summary>
        /// <returns></returns>
        public static IConnectionFactory CreateFactory()
        {
            var config = RabbitMQConnectConfig.GetConfig();
            var factory = new ConnectionFactory()
            {
                HostName = config.HostName,
                Port = config.Port,
                DispatchConsumersAsync = true
            };

            if (!string.IsNullOrWhiteSpace(config.UserName))
            {
                factory.UserName = config.UserName;
            }

            if (!string.IsNullOrWhiteSpace(config.Password))
            {
                factory.Password = config.Password;
            }
            return factory;
        }
    }
}
