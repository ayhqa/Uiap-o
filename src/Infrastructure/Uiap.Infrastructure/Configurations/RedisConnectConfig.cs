﻿namespace Uiap.Infrastructure.Configurations
{
    public class RedisConnectConfig
    {
        /// <summary>
        /// 连接字符串
        /// </summary>
        public string ConnectionString { get; set; } 
    }
}
