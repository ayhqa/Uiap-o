﻿namespace Uiap.Infrastructure.Configurations
{
    public class RabbitMQConnectConfig
    {
        /// <summary>
        /// 服务器地址
        /// </summary>
        public string HostName { get; set; }

        /// <summary>
        /// 端口号
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        public static RabbitMQConnectConfig GetConfig()
        {
            return new RabbitMQConnectConfig
            {
                HostName = "118.24.105.216",
                Port = 5672,
                UserName = "wsy",
                Password = "wsy123"
            };
        }
    }
}
