﻿using System;

namespace Uiap.Infrastructure.Idempotency
{
    /// <summary>
    /// 客户端请求信息
    /// </summary>
    public class ClientRequest
    {
        /// <summary>
        /// 请求唯一ID
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 请求名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 请求时间
        /// </summary>
        public DateTime Time { get; set; }
    }
}
