﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Linq;

namespace Uiap.Infrastructure.Runtime.Caching.HttpRequest
{
    /// <summary>
    /// Http Request缓存
    /// </summary>
    public partial class PerRequestCacheManager : ICacheManager
    {
        private readonly IHttpContextAccessor _accessor;

        private HttpContext Context { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="accessor">Context</param>
        public PerRequestCacheManager(IHttpContextAccessor accessor)
        {
            this._accessor = accessor;
            Context = _accessor.HttpContext;
        }

        /// <summary>
        /// 创建RequestCache实例
        /// </summary>
        protected virtual IDictionary<object, object> GetItems()
        {
            if (Context != null)
                return Context.Items;

            return null;
        }

        /// <summary>
        /// 获取或设置缓存
        /// </summary>
        /// <typeparam name="T">类型</typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public virtual T Get<T>(string key)
        {
            var items = GetItems();
            if (items == null)
                return default(T);
            return (T)items[key];
        }

        /// <summary>
        /// 添加一个缓存对象
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="data">缓存对象</param>
        /// <param name="cacheTime">缓存时间</param>
        public virtual void Set(string key, object data, int cacheTime)
        {
            var items = GetItems();
            if (items == null)
                return;

            if (data != null)
            {
                if (items.Keys.Contains(key))
                    items[key] = data;
                else
                    items.Add(key, data);
            }
        }

        /// <summary>
        /// 判断是否存在对应Key的缓存
        /// </summary>
        /// <param name="key">key</param>
        /// <returns></returns>
        public virtual bool IsSet(string key)
        {
            var items = GetItems();
            if (items == null)
                return false;

            return (items[key] != null);
        }

        /// <summary>
        /// 移除Key对应的缓存对象
        /// </summary>
        /// <param name="key">key</param>
        public virtual void Remove(string key)
        {
            var items = GetItems();
            if (items == null)
                return;

            items.Remove(key);
        }

        /// <summary>
        /// 根据匹配移除
        /// </summary>
        /// <param name="prefix">匹配表达式</param>
        public virtual void RemoveByPrefix(string prefix)
        {
            var items = GetItems();
            if (items == null)
                return;
            var keys = items.Keys.Select(p => p.ToString());
            foreach (var key in keys)
            {
                if (key.StartsWith(prefix))
                    items.Remove(key);
            }
        }

        /// <summary>
        /// 清除所有缓存数据
        /// </summary>
        public virtual void Clear()
        {
            var items = GetItems();
            if (items == null)
                return;

            items.Clear();
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public virtual void Dispose()
        {
        }
    }
}
