﻿using System;

namespace Uiap.Infrastructure.Runtime.Caching
{
    public interface ICacheManager : IDisposable
    {
        /// <summary>
        /// 根据Key获取（有）或设置（无）缓存值.
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="key">缓存对应的唯一Key</param>
        /// <returns>Key对应的缓存值</returns>
        T Get<T>(string key);

        /// <summary>
        /// 添加一个缓存对象
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="data">缓存对象</param>
        /// <param name="cacheTime">缓存时间</param>
        void Set(string key, object data, int cacheTime);

        /// <summary>
        /// 判断是否存在对应Key的缓存
        /// </summary>
        /// <param name="key">key</param>
        /// <returns></returns>
        bool IsSet(string key);

        /// <summary>
        /// 移除Key对应的缓存对象
        /// </summary>
        /// <param name="key">key</param>
        void Remove(string key);

        /// <summary>
        /// 根据匹配移除
        /// </summary>
        /// <param name="prefix">匹配表达式</param>
        void RemoveByPrefix(string prefix);

        /// <summary>
        /// 清除缓存数据
        /// </summary>
        void Clear();
    }
}
