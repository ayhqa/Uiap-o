﻿namespace Uiap.Infrastructure.Runtime.Caching.Redis
{
    public enum RedisDatabaseNumber
    {
        /// <summary>
        /// 缓存数据库
        /// </summary>
        Cache = 1,

        /// <summary>
        /// 密钥保护数据库
        /// </summary>
        DataProtectionKeys = 2
    }
}
