﻿using StackExchange.Redis;
using System;
using System.Net;

namespace Uiap.Infrastructure.Runtime.Caching.Redis
{
    public interface IRedisConnectionWrapper:IDisposable
    {
        /// <summary>
        /// 获取redis数据库交互连接
        /// </summary>
        /// <param name="db">数据库编号</param>
        /// <returns></returns>
        IDatabase GetDatabase(int db);

        /// <summary>
        /// 获取redis独立服务器配置API
        /// </summary>
        /// <param name="endPoint">网络端点</param>
        /// <returns>Redis server</returns>
        IServer GetServer(EndPoint endPoint);

        /// <summary>
        /// 获取服务器所有端点
        /// </summary>
        /// <returns>端点数组</returns>
        EndPoint[] GetEndPoints();

        /// <summary>
        /// 删除数据库所有key
        /// </summary>
        /// <param name="db">数据库编号</param>
        void FlushDatabase(RedisDatabaseNumber db);
    }
}
