﻿using System;

namespace Uiap.Infrastructure.Runtime.Caching
{
    public interface ILocker
    {
        /// <summary>
        /// 使用分布式独占锁（排它锁）执行操作
        /// </summary>
        /// <param name="resource">加锁的Key</param>
        /// <param name="expirationTime">Key过期时间，过期自动释放锁</param>
        /// <param name="action">操作逻辑</param>
        /// <returns></returns>
        bool PerformActionWithLock(string resource, TimeSpan expirationTime, Action action);
    }
}
