﻿using System;

namespace Uiap.Infrastructure
{
    public class IdempotencyException : Exception
    {
        public IdempotencyException()
        { }

        public IdempotencyException(string message)
            : base(message)
        { }

        public IdempotencyException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
