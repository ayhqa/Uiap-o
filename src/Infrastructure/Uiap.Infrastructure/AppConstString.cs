﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uiap.Infrastructure
{
    public class AppConstString
    {
        /// <summary>
        /// redis默认缓存时间,单位：秒(s)
        /// </summary>
        public const int RedisDefaultCacheTime = 60;
    }
}
