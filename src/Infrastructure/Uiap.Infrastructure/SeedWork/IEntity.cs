﻿using MediatR;
using System.Collections.Generic;

namespace Uiap.Infrastructure.SeedWork
{
    /// <summary>
    /// 实体类接口
    /// </summary>
    public interface IEntity
    {
        bool IsTransient();

        void RemoveDomainEvent(INotification eventItem);

        void ClearDomainEvents();

        void AddDomainEvent(INotification eventItem);

        IReadOnlyCollection<INotification> DomainEvents { get;}
    }
}
