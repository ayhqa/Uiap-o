﻿using System;

namespace Uiap.Infrastructure.SeedWork
{
    [Serializable]
    public class ErrorInfo
    {
        /// <summary>
        /// 错误代码
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 错误详情
        /// </summary>
        public string Details { get; set; }
    }
}
