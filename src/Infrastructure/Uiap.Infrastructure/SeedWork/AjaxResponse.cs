﻿namespace Uiap.Infrastructure.SeedWork
{
    public class AjaxResponse
    {
        /// <summary>
        /// 跳转链接
        /// </summary>
        public string TargetUrl { get; set; }

        /// <summary>
        /// 请求是否成功
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// success=false时返回的错误信息
        /// </summary>
        public ErrorInfo Error { get; set; }

        /// <summary>
        /// 返回数据
        /// </summary>
        public dynamic Data { get; set; }

        /// <summary>
        /// 标识是否需要授权访问
        /// </summary>
        public bool UnAuthorizedRequest { get; set; }
    }
}
